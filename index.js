import { Server } from '@hocuspocus/server'
import { RocksDB } from '@hocuspocus/extension-rocksdb'
import { Monitor } from '@hocuspocus/extension-monitor'

let monitor = new Monitor({
  dashboardPath: 'dashboard',
  enableDashboard: true,
  metricsInterval: 10000,
  osMetricsInterval: 10000,
  port: null,
})

const server = Server.configure({
  // async onConnect(data) {
  //   console.log('Connected through document ' + data.documentName)
  // },

  // async onChange(data) {
  //   console.log(data.update)
  // },

  port: 1234,
  extensions: [
    new RocksDB({ path: './database' }),
    monitor,
  ],
})

server.listen()